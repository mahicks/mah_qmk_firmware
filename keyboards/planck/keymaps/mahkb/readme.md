# MAH Planck Keyboard Layout

![MAHKB Layout Diagram](mahkb-keymap.png)

MAHKB is based on the default layout with the following modifications:

* Tab and Esc are swapped (Esc is top-left with Tab below)
* The Backlight key (bottom-left) becomes a new 'Fn' layer switch, most notably
  adding:

  * Home, Pgdn, Pgup and End to the cursor keys (with Backlight becoming
    Fn+Enter)
  * Backspace becomes Delete

That is all.

